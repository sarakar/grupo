class WowzaRest

	def self.check_status(token)
		return true if Rails.env.development?
		begin
			uri = URI.parse("http://107.170.78.48:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/live/instances")
			http = Net::HTTP.new(uri.host, uri.port)
			request = Net::HTTP::Get.new(uri.path, {'Content-Type' =>'application/json'})
			response = http.request(request)
			if response.body.to_s.include? (token)
				return true;
			end
			
			return false
		rescue Exception => e
			return false
		end
	end

	# def self.create_app(token)
	# 	uri = URI.parse("http://localhost:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/#{token}")
	# 	http = Net::HTTP.new(uri.host, uri.port)
	# 	request = Net::HTTP::Post.new(uri.path, {'Content-Type' =>'application/json'})
	# 	data = {
	# 		restURI: "http://localhost:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/#{token}",
	# 		name: "#{token}",
	# 		appType: "Live",
	# 		transcoderEnabled: true,
	# 		clientStreamReadAccess: "*",
	# 		clientStreamWriteAccess: "*",
	# 		applicationTimeout: 0,
	# 		pingTimeout: 0,
	# 		repeaterQueryString: "",
	# 		avSyncMethod: "senderreport",
	# 		httpStreamers: [
	# 			"cupertinostreaming",
	# 			"smoothstreaming",
	# 			"sanjosestreaming",
	# 			"mpegdashstreaming"
	# 		],
	# 		mediaReaderRandomAccessReaderClass: "",
	# 		httpOptimizeFileReads: false,
	# 		mediaReaderBufferSeekIO: false,
	# 		captionLiveIngestType: "",
	# 		description: "Live Streaming",
	# 		streamConfig: {
	# 			restURI: "http://localhost:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/#{token}/streamconfiguration",
	# 			streamType: "live"
	# 	    },

	# 	    transcoderConfig: {
	# 	       restURI: "http://localhost:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/#{token}/transcoder",
	# 	       available: true,
	# 	       licensed: true,
	# 	       licenses: -1,
	# 	       licensesInUse: 0,
	# 	       templates: {
	# 	          restURI: "http://localhost:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/#{token}/transcoder/templates",
	# 	          templates: [
	# 	             {
	# 	                id: "transcode",
	# 	                href: "http://localhost:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/#{token}/transcoder/templates/transcode"
	# 	             },
	# 	             {
	# 	                id: "audioonly",
	# 	                href: "http://localhost:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/#{token}/transcoder/templates/audioonly"
	# 	             },
	# 	             {
	# 	                id: "transcode-h265-divx",
	# 	                href: "http://localhost:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/#{token}/transcoder/templates/transcode-h265-divx"
	# 	             },
	# 	             {
	# 	                id: "transrate",
	# 	                href: "http://localhost:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/#{token}/transcoder/templates/transrate"
	# 	             }
	# 	          ]
	# 	       },
	# 	       liveStreamTranscoder: "transcoder",
	# 	       templatesInUse: "${SourceStreamName}.xml,audioonly.xml",
	# 	       profileDir: "${com.wowza.wms.context.VHostConfigHome}/transcoder/profiles",
	# 	       templateDir: "${com.wowza.wms.context.VHostConfigHome}/transcoder/templates",
	# 	       createTemplateDir: false
	# 	    },
	# 	}
	# 	request.body = data.to_json
	# 	response = http.request(request)
	# 	puts response.body
	# end

	# def self.delete_app(token)
	# 	uri = URI.parse("http://localhost:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/#{token}")
	# 	http = Net::HTTP.new(uri.host, uri.port)
	# 	request = Net::HTTP::Delete.new(uri.path, {'Content-Type' =>'application/json'})
	# 	response = http.request(request)
	# 	puts response.body
	# end

	def self.connections_count(token)
		return 0 if Rails.env.development?
		begin
			uri = URI.parse("http://107.170.78.48:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/live/instances/_definst_/incomingstreams/#{token}/monitoring/current")
			http = Net::HTTP.new(uri.host, uri.port)
			request = Net::HTTP::Get.new(uri.path, {'Content-Type' =>'application/json'})
			response = http.request(request)

			json_response = Hash.from_xml(response.body)
			connectionCount = json_response["CurrentApplicationStatistics"]["TotalConnections"].to_i
			return connectionCount
		rescue Exception => e
			return 0
		end
	end
end