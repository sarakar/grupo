class UserSerializer < ActiveModel::Serializer
  	
  	attributes :id, :username, :skill, :picture_url, :created_at, :updated_at, :phone_number,
  				:token, :status, :email

	def status
	   object.check_status()
	end

	def email
		"sara.email@gmail.com"
	end
end
