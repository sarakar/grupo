ActiveAdmin.register Flag do

# actions :index, :destroy
scope :all
scope :waiting, default: true
scope :processed

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end


	index do
	  column("Flag cause")  do |flag| 
	  	if flag.waiting? 
	  		status_tag(flag.flag_cause.title, :error) 
	  	else
	  		status_tag(flag.flag_cause.title, :ok) 
	  	end
	  end
	  column("Flagged user") do |flag| 

	  	user = User.find_by(id: flag.for_user_id)
	  	if user
	    	link_to(user.username, admin_user_path(user))
	    else
	    	""
		end
	  end 
	  column("Reported By") do |flag| 
	    link_to(flag.user.username, admin_user_path(flag.user))
	  end 
	  column("Action") do |flag|
	    if flag.waiting?
	      link_to 'Stop this User', ignore_admin_flag_path(flag),
	            'data-method' => :put,
	            'data-confirm' => 'Are you sure?',
	            title: 'Stop this User'
	    else
	      "Processed"
	    end
	  end
	  column("Reported at") {|flag| flag.created_at }
	  actions
	end

	member_action :ignore, method: :put do
	  flag = Flag.find_by(id: resource.id)
	  user = User.find_by(id: flag.for_user_id)
	  user.delete_app()

	  related_flags = Flag.where(for_user_id: user.id)
	  related_flags.each do |a_flag|
	  	a_flag.update(status: 1)
	  end

	  redirect_to action: :index
	end


end