ActiveAdmin.register User do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

	permit_params :username, :phone_number, :token, :skill, :picture_url, :email, :verification_status
	actions :index, :show,  :update, :edit, :destroy, :new, :create

	index do
		column("email") do |user| 
			user.email
		end
		column("verification_status") do |user| 
			user.verification_status
		end
		column("Username") do |user| 
			user.username
		end
		column("Skill") do |user| 
		user.skill
		end 
		column("Picture URL") do |user| 
		user.picture_url
		end 
		column("Phone number") do |user|
		user.phone_number
		end
		column("Token") do |user|
		user.token
		end
		column("Status") do |user|
			if user.check_status()
			 	status_tag 'Online', :ok
			else
				status_tag 'Offline', :error
			end 
		end
		actions
	end



end
