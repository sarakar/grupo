class ApplicationMailer < ActionMailer::Base
	
	default from: "grupo.chat.contact@gmail.com"
  	layout 'mailer'

end