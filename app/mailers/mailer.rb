class Mailer < ApplicationMailer 
	default from: "grupo.chat.contact@gmail.com"
	
	def validation_email(user)
		
	    @user = user
	    mail(to: @user.email, subject: 'Verification mail')
    	
  	end

end
