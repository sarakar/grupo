module ApplicationHelper

	def remote_file_exists?(url)
		return false unless url.present? 
		begin
			url = URI.parse(url)
			req = Net::HTTP.new(url.host, url.port)
			req.use_ssl = true if url.scheme == 'https'
			res = req.request_head(url.path)
			return res 
		rescue Exception => e
			return false
		end
	end
end
