

SharedApp = {

	handleCopyToClipboard: function () {
		$(".js-copy-link").click(function() {
			$(".js-link-to-copy").click();
		});

		$(".js-link-to-copy").click(function(e) {
			e.preventDefault();
			var cont = $(this).html(), // Or use a custom source Element
			    $txa = $("<textarea />",{val:cont,css:{position:"fixed"}}).appendTo("body").select(),
			    $msg = $("#clip-popup");
			if(document.execCommand('copy')) $msg.show().delay(1500).fadeOut(); // CH, FF, Edge, IE
			else prompt("Copy to clipboard:\nSelect, Cmd+C, Enter", cont); // Saf, Other
			$txa.remove();
		});
	},

	shareButtons: function () {
		$(".rrssb-buttons").rrssb({
		  // required:
		  /*title: "Grupo chat ",
		  url: "#LiveOnGrupo my link is"+I18N.profile_url,*/

		  // optional:
		  // description: "This is Grupochat app for audio streaming ",
		  // emailBody: "Hello , my profile is"+  I18N.profile_url+". Thanks"
		});
	}, 

	sharePopup: function() {
		$(".js-share-btn").click(function(e) {
			$.magnificPopup.open({
			  items: {
			    src: ".js-share-popup"
			  },
			  type: "inline"
			});

			$(".js-share-popup").css("position", "relative");
			$(".js-share-popup").css("left", "auto");

		});
	}, 

	jwplayer: function  (token, phone_number) {

		isMobile = {
		    Windows: function() {
		        return /IEMobile/i.test(navigator.userAgent);
		    },
		    Android: function() {
		        return /Android/i.test(navigator.userAgent);
		    },
		    BlackBerry: function() {
		        return /BlackBerry/i.test(navigator.userAgent);
		    },
		    iOS: function() {
		        return /iPhone|iPad|iPod/i.test(navigator.userAgent);
		    },
		    any: function() {
		        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows());
		    }
		};

		var file;
		var userAgent = navigator.userAgent || navigator.vendor || window.opera;

		if( isMobile.any() ){
		    file = "http://107.170.78.48:1935/live/"+token+"_aac/playlist.m3u8";
		}else {
		    file = "rtmp://107.170.78.48:1935/live/"+token;
		}

		jwplayer = jwplayer("player").setup({
		    file: file,
		    height: 360,
		    width: 640,

		    skin: {
		       name: "five",
		       active: "#6902E1",
		       inactive: "#6902E1",
		       background: "#f4f4f4"
		    }
		});

		jwplayer.on("setupError", function  () {
			$("#player").remove();
			$(".js-error-flash").show();
		});

		// jwplayer.on("play", function  () {
		// 	$.ajax({
		// 		type: "POST",
		// 		url: "/start_listening",
		// 		data: { listener_phone_number: 0, braodcaster_phone_number: phone_number },
		// 		success: function (response) {
		// 			console.log("****************success******************");
		// 		}
		// 	});
		// });

		// jwplayer.on("pause", function  () {
		// 	$.ajax({
		// 		type: "POST",
		// 		url: "/end_listening",
		// 		data: { listener_phone_number: 0, braodcaster_phone_number: phone_number },
		// 		success: function (response) {
		// 			console.log("****************success******************");
		// 		}
		// 	});
		// });
	},

	tracking: function(username){
		/*console.log("eeeeeeeeeeeeeeeee");*/
		mixpanel.time_event("time_passed");
		window.onbeforeunload = function (e) {
			// If we haven't been passed the event get the window.event
		    e = e || window.event;

		    mixpanel.track("time_passed", {"page": username});
		    mixpanel.time_event("time_passed");

		    var message = '';

		    // For IE6-8 and Firefox prior to version 4
		    if (e) 
		    {
		        e.returnValue = message;
		    }

		    // For Chrome, Safari, IE8+ and Opera 12+
		    return message;
		}
	},

	trackingSignUpClicked: function(){
		mixpanel.track_links("#js-signup","Signup Clicked");
	},

	trackingShareClicked: function(){
		mixpanel.track_links("#js-share","Share Clicked");
	},
	
	detectAppInstalled: function(){
		$(".js-user").click(function() {
			setTimeout(function () { 
			window.location = $('.js-data').data('username'); }, 25);
			window.location = "grupo://grupo.chat?"+$('.js-data').data('username');
		});
	}
}