class Api::V1::UsersController < ActionController::Base

    def update_profile
        result = User.update_profile(params)
        render json: result
    end

    def user_infos
        result = User.user_infos(params)
        render json: result
    end

    def start_listening
        result = User.start_listening(params)
        render json: result
    end

    def listeners
        result = User.listeners(params)
        render json: result
    end

    def end_listening
        result = User.end_listening(params)
        render json: result
    end

    def change_phone_num
        result = User.change_phone_num(params)
        render json: result
    end

    def listeners_number
        result = User.listeners_number(params)
        render json: result
    end

    def get_profile
        result = User.get_profile(params)
        render json: result
    end

    def search_by_username
        result = User.search_by_username(params[:username], params)
        render json: result
    end

    def search_by_phone_num
        result = User.search_by_phone_num(params[:phone_num], params)
        render json: result
    end

    def get_all_users
        result = User.get_all_users(params)
        render json: result
    end

    def get_live_users
        result = User.get_live_users(params)
        render json: result
    end

    def flag_user
        result = User.flag_user(params)
        render json: result
    end

    def get_flags_causes
        result = User.get_flags_causes
        render json: result
    end

    def block_user
        result = User.block_user(params)
        render json: result
    end
end