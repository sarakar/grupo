class Api::V2::BaseController < ApplicationController

	#disable the CSRF token and disable cookies
	protect_from_forgery with: :null_session
	before_action :destroy_session

	def destroy_session
	    request.session_options[:skip] = true
	end

end
