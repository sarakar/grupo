class Api::V2::UsersController < Api::V1::UsersController


    def update_profile
		result = User.update_profile_V2(params)
		render json: result
	end

	def user_infos
    	result = User.user_infos_V2(params)
    	render json: result
    end

    def start_listening
    	result = User.start_listening_V2(params)
    	render json: result
    end

    def listeners
    	result = User.listeners_V2(params)
    	render json: result
    end

    def end_listening
    	result = User.end_listening_V2(params)
    	render json: result
    end

    def change_email
        result = User.change_email_V2(params)
        render json: result
    end

    def listeners_number
        result = User.listeners_number_V2(params)
        render json: result
    end

    def get_profile
        result = User.get_profile_V2(params)
        render json: result
    end

    def search_by_username
        result = User.search_by_username_V2(params[:username], params)
        render json: result
    end

    def search_by_email
        result = User.search_by_email_V2(params[:email], params)
        render json: result
    end

    def get_all_users
        result = User.get_all_users_V2(params)
        render json: result
    end

    def get_live_users
        result = User.get_live_users_V2(params)
        render json: result
    end

    def flag_user
        result = User.flag_user_V2(params)
        render json: result
    end

    def get_flags_causes
        result = User.get_flags_causes
        render json: result
    end

    def block_user
        result = User.block_user_V2(params)
        render json: result
    end

    def login
        result = User.login_V2(params)
        render json: result
    end

end
