class UsersController < ApplicationController


	def profile
		@user = User.find_by(username: params["username"].gsub(/_/, " "))
		TRACKER.track("Anonyme", "Visited the profile of #{@user.username}")
	end

	def confirmation_mail
		user = User.find_by(verification_email_token: params["verification_email_token"])
		if user
			user.verification_status = 1
			user.save
			flash[:notice] = 'You have been verified! Thank You!'
			redirect_to root_path
		else
			flash[:error] = 'An error occurred, please try again later!'
			redirect_to root_path
		end
		
    end

end
