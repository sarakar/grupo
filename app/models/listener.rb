class Listener < ActiveRecord::Base
	belongs_to :user
	belongs_to :listener, class_name: 'User'
end
