require 'rest-client'
class User < ActiveRecord::Base

	validates_format_of :username, with: /\A(?=.*[a-z])[a-z\d\s]+\Z/i
	validates :username, :presence => true, :uniqueness => true
	#validates :email, :presence => true, :uniqueness => true
	#validates :password, :presence => true
	has_many :listeners
	has_many :blocked_users
	has_many :listeners_users, :through => :listeners, :source => "listener"



	# V1

	def self.update_profile(params)
		return {status: 1, message:"A parameter is required"} unless params["phone_number"].present?
		return {status: 1, message:"A parameter is required"} unless params["username"].present?
		return {status: 1, message:"A parameter is required"} unless params["skill"].present?
		# return {status: 1, message:"A parameter is required"} unless params["picture_url"].present?

		user = User.find_by(phone_number: params["phone_number"])
		if user
			user.update(username:params["username"], skill: params["skill"], picture_url: params["picture_url"])
			TRACKER.track(user.id, "Profile update")
		else
			token = User.generate_token
			# WowzaRest.create_app(token)
			user = self.create(phone_number: params["phone_number"], username:params["username"], skill: params["skill"], picture_url: params["picture_url"], token: token)
			TRACKER.track(user.id, "Signup")
		end
		return {status: 1, message: user.errors} if user.errors.any?
		{status:0, user: UserSerializer.new(user, root: false)}
	end

	def self.user_infos(params)
		user_status = ""
		return {status: 1, message:"Phone number is required"} unless params["phone_number"].present?
		user = User.find_by(phone_number: params["phone_number"])
		if user
			TRACKER.track(user.id, "Get user infos")
			return {status:0, user: UserSerializer.new(user, root: false)}
		else
			return {status:1, message:"User not found"}
		end
	end

	def self.get_profile(params)
		return {status: 1, message:"Username is required"} unless params["username"].present?
		user = User.find_by(username: params["username"])
		if user
			TRACKER.track(user.id, "Get user profile from username")
			return {status:0, user: UserSerializer.new(user, root: false)}
		else
			return {status:1, message:"User not found"}
		end
	end

	def self.start_listening(params)

		return {status: 1, message:"Broadcaster's phone number is required"} unless  params["braodcaster_phone_number"].present? 
		return {status: 1, message:"listener's phone number is required"} unless  params["listener_phone_number"].present?
		
		user =  User.find_by(phone_number: params["braodcaster_phone_number"])

		listener =  User.find_by(phone_number: params["listener_phone_number"])
		
		if params["listener_phone_number"].to_s == "0"
			#listener = 
		end


		if user and listener 
			listeners = ActiveModel::ArraySerializer.new(user.listeners_users, each_serializer: ListenerSerializer)
			Listener.find_or_create_by(user_id: user.id, listener_id: listener.id)
			TRACKER.track(listener.id, "Start listening")
			return {status:0, user: UserSerializer.new(user, root: false), listeners: ActiveModel::ArraySerializer.new(user.listeners_users, each_serializer: UserSerializer)}
		else
			{status:1, message:"User not found"}
		end
	end


	def self.listeners(params)

		return {status: 1, message:"Broadcaster's phone number is required"} unless  params["braodcaster_phone_number"].present? 
		
		user = User.find_by(phone_number: params["braodcaster_phone_number"])

		if user
			return {status:0, listeners: ActiveModel::ArraySerializer.new(user.listeners_users, each_serializer: UserSerializer)}
		else
			{status:1, message:"User not found"}
		end
	end


	def self.end_listening(params)

		return {status: 1, message:"Broadcaster's phone number is required"} unless  params["braodcaster_phone_number"].present? 
		return {status: 1, message:"listener's phone number is required"} unless  params["listener_phone_number"].present?
		
		user =  User.find_by(phone_number: params["braodcaster_phone_number"])
		listener =  User.find_by(phone_number: params["listener_phone_number"])

		if user and listener
			list = Listener.find_by(user_id: user.id, listener_id: listener.id)
			TRACKER.track(listener.id, "Stop listening")
			list.delete if list
			return {status:0, user: UserSerializer.new(user, root: false), listeners: ActiveModel::ArraySerializer.new(user.listeners_users, each_serializer: UserSerializer)}
		else
			{status:1, message:"User not found"}
		end
	end

	def self.generate_token
		token = ""
		found = true
		while found
			token = SecureRandom.hex(10)
			found = !!User.find_by(token: token)
		end
		token
	end


	def self.change_phone_num(params)
		return {status: 1, message:"Phone number is required"} unless params["phone_number"].present?
		return {status: 1, message:"The new phone number is required"} unless params["phone_number_new"].present?
		user = User.find_by(phone_number:params["phone_number"])
		user_2 = User.find_by(phone_number:params["phone_number_new"])
		if user
			if user_2
				return {status:1, message:"The new phone number is already used"}
			else
				user.update(phone_number:params["phone_number_new"])
				return {status:0, user: UserSerializer.new(user, root: false)} if user
			end
		else
			return {status:1, message:"User not found"}
		end
	end

	def self.listeners_number(params)
		return {status: 1, message:"Broadcaster's phone number is required"} unless  params["braodcaster_phone_number"].present? 
				
		user = User.find_by(phone_number: params["braodcaster_phone_number"])

		if user
			return {status:0, listeners_number: WowzaRest.connections_count(user.token)}
		else
			{status:1, message:"User not found"}
		end
	end


	def check_status()
		WowzaRest.check_status(self.token)
	end
	

	def self.search_by_username(username, params)
		users = []
		if username
			users= User.where("username LIKE ?", "#{username}%") 
		end

		if  params["user_phone_number"].present?
			sender_user = User.find_by(phone_number: params["user_phone_number"]) 
			users = users.where.not(id: sender_user.blocked_users.pluck(:blocked_user_id))
		end
		
		users = ActiveModel::ArraySerializer.new(users, each_serializer: UserSerializer)
		return {status:0, users: users}
	end

	def self.search_by_phone_num(phone_num, params)
		users = []

		if phone_num
			users= User.where("phone_number LIKE ?", "%#{phone_num}%") 
		end

		if  params["user_phone_number"].present?
			sender_user = User.find_by(phone_number: params["user_phone_number"]) 
			users = users.where.not(id: sender_user.blocked_users.pluck(:blocked_user_id))
		end

		users = ActiveModel::ArraySerializer.new(users, each_serializer: UserSerializer)
		return {status:0, users: users}
	end

	def self.get_all_users(params)
		users = []
		users= User.all
		
		if  params["user_phone_number"].present?
			sender_user = User.find_by(phone_number: params["user_phone_number"]) 
			users = users.where.not(id: sender_user.blocked_users.pluck(:blocked_user_id))
		end

		users = ActiveModel::ArraySerializer.new(users, each_serializer: UserSerializer)
		return {status:0, users: users}
	end

	def self.get_live_users(params)
		users = []
		live_users = []
		offline_users = []
		users= User.all
		
		sender_user = nil
		
		if  params["user_phone_number"].present?
			sender_user = User.find_by(phone_number: params["user_phone_number"]) 
		end
		
		users.each do |user|
			next if sender_user and sender_user.blocked_users.pluck(:blocked_user_id).include? user.id
			if user.check_status()
				live_users << user
			else
				offline_users << user
			end
		end


		live_users = ActiveModel::ArraySerializer.new(live_users, each_serializer: UserSerializer)
		offline_users = ActiveModel::ArraySerializer.new(offline_users, each_serializer: UserSerializer)

		return {status:0, live_users: live_users, offline_users: offline_users }
	end

	def self.flag_user(params)
		return {status: 1, message:"Broadcaster's phone number is required"} unless  params["braodcaster_phone_number"].present? 
		return {status: 1, message:"user's phone number is required"} unless  params["user_phone_number"].present?
		return {status: 1, message:"flag_cause_id is required"} unless  params["flag_cause_id"].present?
		
		broadcaster =  User.find_by(phone_number: params["braodcaster_phone_number"])
		user =  User.find_by(phone_number: params["user_phone_number"])
		flag_cause = FlagCause.find_by(id: params["flag_cause_id"])

		if user and broadcaster and flag_cause

			related_flags = Flag.where(for_user_id: broadcaster.id)

			if related_flags.size == 4
				broadcaster.delete_app() 
				related_flags.each do |a_flag|
					a_flag.update(status: 1)
				end
			end

		    report = Flag.create_report(flag_cause, broadcaster, user)
			TRACKER.track(user.id, "Flagged the user with id: #{broadcaster.id}")
			return {status:0, message: "User Flagged"}
		else
			{status:1, message:"User or flag id not found"}
		end
	end

	def self.get_flags_causes
		{status:1, flages_causes: FlagCause.all}
	end

	def delete_app()
		WowzaRest.delete_app(self.token)
	end

	def self.block_user(params)
		return {status: 1, message:"Broadcaster's phone number is required"} unless  params["braodcaster_phone_number"].present? 
		return {status: 1, message:"user's phone number is required"} unless  params["user_phone_number"].present?
		
		broadcaster =  User.find_by(phone_number: params["braodcaster_phone_number"])
		user =  User.find_by(phone_number: params["user_phone_number"])
		
		if user and broadcaster

			BlockedUser.create(user_id: user.id, blocked_user_id: broadcaster.id)

			TRACKER.track(user.id, "Blocked the user with id: #{broadcaster.id}")
			return {status:0, message: "User Blocked"}
		else
			{status:1, message:"User not found"}
		end
	end


	# **************************   V2




	def self.create_hashed_password(password)
		salt = SecureRandom.base64(8)
		{hashed_password: Digest::SHA2.hexdigest(salt + password), salt: salt}
	end

	def password_correct?(hashed_password, password_to_confirm, salt)
  		hashed_password == Digest::SHA2.hexdigest(salt + password_to_confirm)
	end

	def self.login_V2(params)
		
		return {status: 1, message:"email is required"} unless params["email"].present?
		return {status: 1, message:"password is required"} unless params["password"].present?

		user = User.find_by(username: params["email"], password: params["password"])

		if user
			{status:0, user: UserSerializer.new(user, root: false)}
		else
			{status:1, message:"user not found"}
		end
	end

	def self.update_profile_V2(params)
		# return {status: 1, message:"A parameter is required"} unless params["phone_number"].present?
		return {status: 1, message:"The email is required"} unless params["email"].present?
		return {status: 1, message:"The username is required"} unless params["username"].present?
		return {status: 1, message:"The skill is  required"} unless params["skill"].present?
		return {status: 1, message:"The password is required"} unless params["password"].present?

		user = User.find_by(email: params["email"])
		if user
			user.update(email:params["email"],username:params["username"], skill: params["skill"], picture_url: params["picture_url"], password: params["password"])
			TRACKER.track(user.id, "Profile update")
		else
			verification_email_token = User.generate_token
			token = User.generate_token
			# WowzaRest.create_app(token)
			user = self.create(email:params["email"],username:params["username"], skill: params["skill"], picture_url: params["picture_url"], token: token, verification_email_token: verification_email_token, verification_status: 0, password: params["password"])
			if user.valid?
				# Mailer.validation_email(user).deliver
				RestClient.post "https://api:key-c79f121b95331927c9082ae7c1274bbe"\
				  "@api.mailgun.net/v3/grupo.live/messages",
				  :from => "Grupo Contact <contact@grupo.live>",
				  :to => user.email,
				  :subject => "Account Activation",
				  :html => '<!DOCTYPE html><html><head><meta content=\'text/html; charset=UTF-8\' http-equiv=\'Content-Type\' /></head><body><h1>Hello '+ user.username+',</h1><p>You can confirm your account email through the link below:</p><p><a href="http://grupo.live/confirmation_mail?verification_email_token='+user.verification_email_token+'">http://grupo.live/confirmation_mail?verification_email_token='+ user.verification_email_token+'</a></p></body></html>'
			end
			TRACKER.track(user.id, "Signup")
		end
		return {status: 1, message: user.errors} if user.errors.any?
		{status:0, user: UserSerializer.new(user, root: false)}
	end


	def self.user_infos_V2(params)
		user_status = ""
		return {status: 1, message:"The email is required"} unless params["email"].present?
		user = User.find_by(email: params["email"])
		if user
			TRACKER.track(user.id, "Get user infos")
			return {status:0, user: UserSerializer.new(user, root: false)}
		else
			return {status:1, message:"User not found"}
		end
	end

	def self.get_profile_V2(params)
		return {status: 1, message:"Username is required"} unless params["username"].present?
		user = User.find_by(username: params["username"])
		if user
			TRACKER.track(user.id, "Get user profile from username")
			return {status:0, user: UserSerializer.new(user, root: false)}
		else
			return {status:1, message:"User not found"}
		end
	end

	def self.start_listening_V2(params)

		return {status: 1, message:"Broadcaster's email is required"} unless  params["braodcaster_email"].present? 
		return {status: 1, message:"listener's email is required"} unless  params["listener_email"].present?
		
		user =  User.find_by(email: params["braodcaster_email"])

		listener =  User.find_by(email: params["listener_email"])
		
		if params["listener_email"] == "0"
		
		end

		if user and listener 
			listeners = ActiveModel::ArraySerializer.new(user.listeners_users, each_serializer: ListenerSerializer)
			Listener.find_or_create_by(user_id: user.id, listener_id: listener.id)
			TRACKER.track(listener.id, "Start listening")
			return {status:0, user: UserSerializer.new(user, root: false), listeners: ActiveModel::ArraySerializer.new(user.listeners_users, each_serializer: UserSerializer)}
		else
			{status:1, message:"User not found"}
		end
	end

	def self.listeners_V2(params)

		return {status: 1, message:"Broadcaster's email is required"} unless  params["braodcaster_email"].present? 
		
		user = User.find_by(email: params["braodcaster_email"])

		if user
			return {status:0, listeners: ActiveModel::ArraySerializer.new(user.listeners_users, each_serializer: UserSerializer)}
		else
			{status:1, message:"User not found"}
		end
	end

	def self.end_listening_V2(params)

		return {status: 1, message:"Broadcaster's email is required"} unless  params["braodcaster_email"].present? 
		return {status: 1, message:"listener's email is required"} unless  params["listener_email"].present?
		
		user =  User.find_by(email: params["braodcaster_email"])
		listener =  User.find_by(email: params["listener_email"])

		if user and listener
			list = Listener.find_by(user_id: user.id, listener_id: listener.id)
			TRACKER.track(listener.id, "Stop listening")
			list.delete if list
			return {status:0, user: UserSerializer.new(user, root: false), listeners: ActiveModel::ArraySerializer.new(user.listeners_users, each_serializer: UserSerializer)}
		else
			{status:1, message:"User not found"}
		end
	end

	def self.change_email_V2(params)
		return {status: 1, message:"email is required"} unless params["email"].present?
		return {status: 1, message:"The new email is required"} unless params["email_new"].present?
		user = User.find_by(email:params["email"])
		user_2 = User.find_by(email:params["email_new"])
		if user
			if user_2
				return {status:1, message:"The new email is already used"}
			else
				user.update(email:params["email_new"])
				return {status:0, user: UserSerializer.new(user, root: false)} if user
			end
		else
			return {status:1, message:"User not found"}
		end
	end

	def self.listeners_number_V2(params)
		return {status: 1, message:"Broadcaster's email is required"} unless  params["braodcaster_email"].present? 
				
		user = User.find_by(email: params["braodcaster_email"])

		if user
			return {status:0, listeners_number: WowzaRest.connections_count(user.token)}
		else
			{status:1, message:"User not found"}
		end
	end

	def self.search_by_username_V2(username, params)
		users = []
		if username
			users= User.where("username LIKE ?", "#{username}%") 
		end

		if  params["user_email"].present?
			sender_user = User.find_by(email: params["user_email"]) 
			users = users.where.not(id: sender_user.blocked_users.pluck(:blocked_user_id))
		end
		
		users = ActiveModel::ArraySerializer.new(users, each_serializer: UserSerializer)
		return {status:0, users: users}
	end

	def self.search_by_email_V2(email, params)
		users = []
		if email
			users= User.where("email LIKE ?", "%#{email}%") 
		end

		if  params["user_email"].present?
			sender_user = User.find_by(email: params["user_email"]) 
			users = users.where.not(id: sender_user.blocked_users.pluck(:blocked_user_id))
		end

		users = ActiveModel::ArraySerializer.new(users, each_serializer: UserSerializer)
		return {status:0, users: users}
	end

	def self.get_all_users_V2(params)
		users = []
		users= User.all
		
		if  params["user_email"].present?
			sender_user = User.find_by(email: params["user_email"]) 
			users = users.where.not(id: sender_user.blocked_users.pluck(:blocked_user_id))
		end

		users = ActiveModel::ArraySerializer.new(users, each_serializer: UserSerializer)
		return {status:0, users: users}
	end

	def self.get_live_users_V2(params)
		users = []
		live_users = []
		offline_users = []
		users= User.all
		
		sender_user = nil
		
		if  params["user_email"].present?
			sender_user = User.find_by(email: params["user_email"]) 
		end
		
		users.each do |user|
			next if sender_user and sender_user.blocked_users.pluck(:blocked_user_id).include? user.id
			if user.check_status()
				live_users << user
			else
				offline_users << user
			end
		end

		live_users = ActiveModel::ArraySerializer.new(live_users, each_serializer: UserSerializer)
		offline_users = ActiveModel::ArraySerializer.new(offline_users, each_serializer: UserSerializer)

		return {status:0, live_users: live_users, offline_users: offline_users }
	end

	def self.flag_user_V2(params)
		return {status: 1, message:"Broadcaster's email is required"} unless  params["braodcaster_email"].present? 
		return {status: 1, message:"user's email is required"} unless  params["user_email"].present?
		return {status: 1, message:"flag_cause_id is required"} unless  params["flag_cause_id"].present?
		
		broadcaster =  User.find_by(email: params["braodcaster_email"])
		user =  User.find_by(email: params["user_email"])
		flag_cause = FlagCause.find_by(id: params["flag_cause_id"])

		if user and broadcaster and flag_cause

			related_flags = Flag.where(for_user_id: broadcaster.id)

			if related_flags.size == 4
				broadcaster.delete_app() 
				related_flags.each do |a_flag|
					a_flag.update(status: 1)
				end
			end

		    report = Flag.create_report(flag_cause, broadcaster, user)
			TRACKER.track(user.id, "Flagged the user with id: #{broadcaster.id}")
			return {status:0, message: "User Flagged"}
		else
			{status:1, message:"User or flag id not found"}
		end
	end

	def self.block_user_V2(params)
		return {status: 1, message:"Broadcaster's email  is required"} unless  params["braodcaster_email"].present? 
		return {status: 1, message:"user's email is required"} unless  params["user_email"].present?
		
		broadcaster =  User.find_by(email: params["braodcaster_email"])
		user =  User.find_by(email: params["user_email"])
		
		if user and broadcaster

			BlockedUser.create(user_id: user.id, blocked_user_id: broadcaster.id)

			TRACKER.track(user.id, "Blocked the user with id: #{broadcaster.id}")
			return {status:0, message: "User Blocked"}
		else
			{status:1, message:"User not found"}
		end
	end

end