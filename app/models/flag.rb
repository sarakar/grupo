class Flag < ActiveRecord::Base
	belongs_to :user
	belongs_to :flag_cause

	enum status: [:waiting, :processed]

  	class << self
		def create_report(flag_cause, broadcaster, user)
			report = self.where(for_user_id: broadcaster.id,
	                      		user_id: user.id).first

			return report if report
			report = self.create(for_user_id: broadcaster.id,
			    				 flag_cause_id: flag_cause.id,
			                     user_id: user.id,
			                     status: 0)
			report
		end
	end
end
