class AddTokenToUser < ActiveRecord::Migration
  
  def self.up
  	add_column :users, :token, :text
  end

end