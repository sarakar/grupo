class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :skill
      t.text :picture_url

      t.timestamps null: false
    end
  end
end
