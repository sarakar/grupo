class CreateFlagCauses < ActiveRecord::Migration
  def change
    create_table :flag_causes do |t|
      t.string :title

      t.timestamps null: false
    end
  end
end
