class AddVerificationEmailTokenToUsers < ActiveRecord::Migration
  def change
    add_column :users, :verification_email_token, :string
  end
end
