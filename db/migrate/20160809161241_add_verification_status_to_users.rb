class AddVerificationStatusToUsers < ActiveRecord::Migration
  def change
    add_column :users, :verification_status, :integer
  end
end
