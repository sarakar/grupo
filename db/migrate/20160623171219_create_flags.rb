class CreateFlags < ActiveRecord::Migration
  def change
    create_table :flags do |t|
      t.integer :user_id
      t.integer :flag_cause_id
      t.integer :for_user_id

      t.timestamps null: false
    end
  end
end
