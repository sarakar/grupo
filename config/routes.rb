Rails.application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  
  #api v1
  namespace :api , :defaults => { :format => 'json' } do
    scope :module => :v1 do

      # ************flag****************
      post "flag_user", to: 'users#flag_user'
      get "get_flags_causes", to: 'users#get_flags_causes'
      post "block_user", to: 'users#block_user'
      post "user_infos", to: 'users#user_infos'
      post "get_profile", to: 'users#get_profile'
      post 'update_profile', to: 'users#update_profile'
      post 'start_listening', to: 'users#start_listening'
      post 'end_listening', to: 'users#end_listening'
      post 'change_email', to: 'users#change_email'
      post 'search_by_username', to: 'users#search_by_username'
      post 'search_by_phone_num', to: 'users#search_by_phone_num'
      get 'listeners_number', to: 'users#listeners_number'
      get 'listeners', to: 'users#listeners'
      post 'get_all_users', to: 'users#get_all_users'
      post 'get_live_users', to: 'users#get_live_users'
      post 'login', to: 'users#login'
      
    end
  
    namespace  :v2 do

      # ************flag****************
      post "flag_user", to: 'users#flag_user'
      get "get_flags_causes", to: 'users#get_flags_causes'
      post "block_user", to: 'users#block_user'
      post "user_infos", to: 'users#user_infos'
      post "get_profile", to: 'users#get_profile'
      post 'update_profile', to: 'users#update_profile'
      post 'start_listening', to: 'users#start_listening'
      post 'end_listening', to: 'users#end_listening'
      post 'change_email', to: 'users#change_email'
      post 'search_by_username', to: 'users#search_by_username'
      post 'search_by_email', to: 'users#search_by_email'
      get 'listeners_number', to: 'users#listeners_numbe'
      get 'listeners', to: 'users#listeners'
      post 'get_all_users', to: 'users#get_all_users'
      post 'get_live_users', to: 'users#get_live_users'
      post 'login', to: 'users#login'
      
    end

      # match 'v:api/*path', :to => redirect("/api/v1/%{path}"), via: [:get, :post]
      # match '*path', :to => redirect("/api/v1/%{path}"), via: [:get, :post]
  end

  #web
    root 'globals#index'
    get '/confirmation_mail' => 'users#confirmation_mail', :as => :confirmation_mail
    get '/:username' => 'users#profile', :as => :profile

end

